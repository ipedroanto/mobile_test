import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { TestRoutingModule } from "./tests-routing.module";
import { TestComponent } from "./test.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        TestRoutingModule
    ],
    declarations: [
        TestComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class TestModule { }